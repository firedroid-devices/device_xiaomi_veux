echo "Cloning required stuffs..."

echo "Cloning Kernel tree..."
# Kernel
git clone https://github.com/MrTopia/kernel_xiaomi_sm6375 kernel/xiaomi/sm6375 --depth=1

echo "Cloning Firmware tree..."
# Firmware Tree
git clone https://gitlab.com/MrTopia/vendor_xiaomi_veux-firmware.git vendor/xiaomi/veux-firmware

echo "Cloning Vendor..."
# Vendor
git clone https://gitlab.com/MrTopia/vendor_xiaomi_veux.git vendor/xiaomi/veux

echo "Cloning hardware related stuff..."
# Hardware
git clone https://github.com/Evolution-X/hardware_xiaomi hardware/xiaomi

echo "Cloning Gcam stuff..."
# Gcam
git clone https://github.com/MrTopia/vendor_xiaomi_veux-gcam vendor/xiaomi/veux-gcam

echo "Cloning ViPER4AndroidFX..."
# ViPER4Android
git clone https://github.com/TogoFire/packages_apps_ViPER4AndroidFX packages/apps/ViPER4AndroidFX

echo 'Completed, proceeding to lunch'
